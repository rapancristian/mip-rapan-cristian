package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController implements Initializable {

	private String user = "Chris";
	private String password = "1234";
	
	@FXML
	private Button close;

	@FXML
	private Button loginButton;
	
	@FXML
	private Label statusLabel;
	
	@FXML
	private TextField userField;
	
	@FXML
	private PasswordField passwordField;
	
	@FXML
	private void closeButtonAction(){
	    // get a handle to the stage
	    Stage stage = (Stage) close.getScene().getWindow();
	    // do what you have to do
	    stage.close();
	}
	
	@FXML
	private void loginActionButton() throws IOException {
		if(userField.getText().equals(user) && passwordField.getText().equals(password)) {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("MainView.fxml"));
			Scene scene = new Scene(fxmlLoader.load(), 800, 800);
			Stage stage = new Stage();
			stage.setTitle("PetShop");
			stage.setScene(scene);
			stage.show();
		} else {
			statusLabel.setText("Login failed!");
		}
	}
	

	@FXML
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX 2 Login");

		// Implementing Nodes for GridPane

		Label lblUserName = new Label("Username");

		final TextField txtUserName = new TextField();

		Label lblPassword = new Label("Password");

		final PasswordField pf = new PasswordField();
		Button btnLogin = new Button("Login");

		final Label lblMessage = new Label();


	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
}