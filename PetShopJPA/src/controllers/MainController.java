/*
 * 
 */
package controllers;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.Animal;
import model.PersonalMedical;
import model.Programare;
import model.ProgramarePK;
import util.DataBaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<String > listView;
	@FXML
	private ListView<String> animalListView;
	@FXML
	private ListView<Integer> animalIdListView;
	@FXML
	private ListView<String> animalSpecieListView;
	@FXML
	private ListView<String> personalMedicalNumeListView;
	@FXML
	private ListView<String> personalMedicalSpecializareListView;
	@FXML
	private ListView<Integer> personalMedicalIdListView;
	@FXML
	private ListView<Date> programareDataListView;
	@FXML
	private ListView<Integer> programareIDListView;
	@FXML
	private ListView<Integer> programareAnimalId;
	@FXML
	private ListView<Integer> programarePersonalId;
	@FXML
	private ChoiceBox<String> animalSpecieChoiceBox ;
	@FXML
	private TextField animalName;
	@FXML
	private TextField createAnimalId;
	@FXML
	private TextField personalMedicalNume;
	@FXML
	private ChoiceBox<String> personalMedicalSpecializare;
	@FXML
	private TextField personalMedicalId;
		
		//Populates the animal panel
		private void populateAnimalListView() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			List<Animal> animalDBList = (List <Animal>) DataBaseUtil.getInstance().animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		ObservableList<Integer> animalIdList = getAnimalId(animalDBList);
	//	ObservableList<String> animalSpecieList = getAnimalSpecie(animalDBList);
		animalSpecieChoiceBox.setItems(getAvailableSpecies());
		animalListView.setItems(animalNamesList);
		animalIdListView.setItems(animalIdList);
	//	animalSpecieListView.setItems(animalSpecieList);
		animalListView.refresh();
		animalIdListView.refresh();
		DataBaseUtil.getInstance().closeEntityManager();
		}
		
		//sadasdasd
		
		//Populates the personal medical panel
		private void populatePersonalMedicalListView() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			personalMedicalSpecializare.setItems(getAvailableSpecies());
			List<PersonalMedical> personalMedicalDBList = (List <PersonalMedical>) 
					DataBaseUtil.getInstance().personalMedicalList();
			ObservableList<String> personalMedicalNamesList = getPersonalMedicalNume(personalMedicalDBList);
			//ObservableList<String> personalMedicalSpecializareList = getPersonalMedicalSpecializare(personalMedicalDBList);
			ObservableList<Integer> personalMedicalIdList = 
					getPersonalMedicalId(personalMedicalDBList);
			personalMedicalNumeListView.setItems(personalMedicalNamesList);
			personalMedicalNumeListView.refresh();
		//	personalMedicalSpecializareListView.setItems(personalMedicalSpecializareList);
			personalMedicalSpecializareListView.refresh();
			personalMedicalIdListView.setItems(personalMedicalIdList);
			personalMedicalIdListView.refresh();
			
			DataBaseUtil.getInstance().closeEntityManager();
		}

		// Populates programare listView
		private void populateProgramareListView() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			List<Programare> programareDBList = (List <Programare>) 
					DataBaseUtil.getInstance().programareList();
		//	ObservableList<Date> programareDateList = getProgramareDate(programareDBList);
			ObservableList<Integer> programareIdList = getProgramareId(programareDBList);
			ObservableList<Integer> programareIdAnimalList = getProgramareAnimalId(programareDBList);
			ObservableList<Integer> programareIdPersonalMedicalList = 
					getProgramarePersonalMedicalId(programareDBList);
		//	programareDataListView.setItems(programareDateList);
		//	programareDataListView.refresh();
			programareIDListView.setItems(programareIdList);
			programareIDListView.refresh();
			programareAnimalId.setItems(programareIdAnimalList);
			programareAnimalId.refresh();
		programarePersonalId.setItems(programareIdPersonalMedicalList);
			programarePersonalId.refresh();
			
			DataBaseUtil.getInstance().closeEntityManager();
			
		}
		//creates an animal when button is pushed


		//creates an animal
		@FXML
		private void createAnimal() throws Exception {
			Animal myAnimal = new Animal();
			myAnimal.setName(animalName.getText());
			myAnimal.setIdAnimal(Integer.parseInt(createAnimalId.getText()));
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			DataBaseUtil.getInstance().saveAnimal(myAnimal);
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			populateAnimalListView();

		}

		//updates an animal
		@FXML
		private void updateAnimal() throws Exception {
			Animal myAnimal = new Animal();
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			myAnimal = DataBaseUtil.getInstance().findAnimal(Integer.parseInt(createAnimalId.getText()));
			myAnimal.setName(animalName.getText());
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			populateAnimalListView();
			
		}

		//deletes an animal
		@FXML
		private void deleteAnimal() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			Animal myAnimal = DataBaseUtil.getInstance().findAnimal(Integer.parseInt(createAnimalId.getText()));
			DataBaseUtil.getInstance().entityManager.remove(myAnimal);
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			populateAnimalListView();
			
		}

		//creates personal medical
		@FXML
		private void createPersonalMedical() throws Exception {
			PersonalMedical myPersonalMedical = new PersonalMedical();
			myPersonalMedical.setName(personalMedicalNume.getText());
			myPersonalMedical.setIdpersonalmedical(Integer.parseInt(personalMedicalId.getText()));
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			DataBaseUtil.getInstance().savePersonalMedical(myPersonalMedical);
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			
		}
		//updates personal medical found by id
		@FXML
		private void updatePersonalMedical() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			PersonalMedical myPersonalMedical = DataBaseUtil.getInstance().findPersonalMedical
					(Integer.parseInt(personalMedicalId.getText()));
			myPersonalMedical.setName(personalMedicalNume.getText());
			DataBaseUtil.getInstance().savePersonalMedical(myPersonalMedical);
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			
		}

		//deletes a personal medical found by id
		@FXML
		private void deletePersonalMedical() throws Exception {
			DataBaseUtil.getInstance().setUp();
			DataBaseUtil.getInstance().startTransaction();
			PersonalMedical myPersonalMedical = DataBaseUtil.getInstance().findPersonalMedical(Integer.parseInt(personalMedicalId.getText()));
			DataBaseUtil.getInstance().entityManager.remove(myPersonalMedical);
			DataBaseUtil.getInstance().commitTransaction();
			DataBaseUtil.getInstance().printAllAnimalsFromDB();
			DataBaseUtil.getInstance().closeEntityManager();
			
		}



		//getter for all animals species available
		public ObservableList<String> getAvailableSpecies (){
			ObservableList<String> species = FXCollections.observableArrayList("Caine","Pisica","Hamster","Cal");
			return species;
		}


		//getter for all animal id
		public ObservableList<Integer> getAnimalId (List<Animal> animals){
			ObservableList<Integer> id = FXCollections.observableArrayList();
			for(Animal a: animals) {
				id.add(a.getIdAnimal());
			}
			return id;
		}
		//getter for all personal medical name
		public ObservableList<String> getPersonalMedicalNume(List<PersonalMedical> personalMedicalList){
			ObservableList<String> nume = FXCollections.observableArrayList();
			for(PersonalMedical p : personalMedicalList){
				nume.add(p.getName());
			}
			return nume;
		}

		
		//getter for all personal medical id
		public ObservableList<Integer> getPersonalMedicalId (List<PersonalMedical> personalMedicalList){
			ObservableList<Integer> id = FXCollections.observableArrayList();
			for(PersonalMedical p : personalMedicalList) {
				id.add(p.getIdpersonalmedical());
			}
			return id;
		}
		
		//getter for all programare id 
		public ObservableList<Integer> getProgramareId(List<Programare> programareList){
			ObservableList<Integer> id = FXCollections.observableArrayList();
			for(Programare p : programareList) {
				id.add(p.getId().getIdProgramare());
			}
			return id;
		}

		//getter for all programare animal id
		public ObservableList<Integer> getProgramareAnimalId(List<Programare> programareList){
			ObservableList<Integer> id = FXCollections.observableArrayList();
			for(Programare p : programareList) {
				id.add(p.getAnimal().getIdAnimal());
			}
			return id;
		}
		
		//getter for all programare personal medical id
		public ObservableList<Integer> getProgramarePersonalMedicalId(List<Programare> programareList){
			ObservableList<Integer> id = FXCollections.observableArrayList();
			for(Programare p : programareList) {
				id.add(p.getPersonalmedical().getIdpersonalmedical());
			}
			return id;
		}
		


		@Override
		public void initialize(URL location, ResourceBundle resources) {
			try {
				populateAnimalListView();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				populatePersonalMedicalListView();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				populateProgramareListView();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//asdasdasdasd

		
	
	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());
		}
		return names;
	}
	
	/*
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	*/

}
