/*
 *  @author Cristi
 *  @name Animal
 */
package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;



/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id animal. */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	/** The name. */
	private String name;

	/** The programares. */
	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal", orphanRemoval = true, cascade = CascadeType.PERSIST)
	private List<Programare> programares;

	/**
	 * Instantiates a new animal.
	 */
	public Animal() {
	}

	/**
	 * Gets the id animal.
	 *
	 * @return the id animal
	 */
	public int getIdAnimal() {
		return this.idAnimal;
	}

	/**
	 * Sets the id animal.
	 *
	 * @param idAnimal the new id animal
	 */
	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

}