package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the programare database table.
 * 
 */
@Embeddable
public class ProgramarePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int idProgramare;

	@Column(insertable=false, updatable=false)
	private int animal_idAnimal;

	@Column(name="personalmedical_idpersonalmedical", insertable=false, updatable=false)
	private int personalmedicalIdpersonalmedical;

	public ProgramarePK() {
	}
	public int getIdProgramare() {
		return this.idProgramare;
	}
	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}
	public int getAnimal_idAnimal() {
		return this.animal_idAnimal;
	}
	public void setAnimal_idAnimal(int animal_idAnimal) {
		this.animal_idAnimal = animal_idAnimal;
	}
	public int getPersonalmedicalIdpersonalmedical() {
		return this.personalmedicalIdpersonalmedical;
	}
	public void setPersonalmedicalIdpersonalmedical(int personalmedicalIdpersonalmedical) {
		this.personalmedicalIdpersonalmedical = personalmedicalIdpersonalmedical;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProgramarePK)) {
			return false;
		}
		ProgramarePK castOther = (ProgramarePK)other;
		return 
			(this.idProgramare == castOther.idProgramare)
			&& (this.animal_idAnimal == castOther.animal_idAnimal)
			&& (this.personalmedicalIdpersonalmedical == castOther.personalmedicalIdpersonalmedical);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idProgramare;
		hash = hash * prime + this.animal_idAnimal;
		hash = hash * prime + this.personalmedicalIdpersonalmedical;
		
		return hash;
	}
}