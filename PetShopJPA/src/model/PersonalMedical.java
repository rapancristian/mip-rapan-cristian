/*
 * 
 */
package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 * The persistent class for the PersonalMedical database table.
 * 
 */
@Entity
@NamedQuery(name="PersonalMedical.findAll", query="SELECT p FROM PersonalMedical p")
public class PersonalMedical implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idPersonalMedical. */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idpersonalmedical;

	
	/** The name. */
	private String name;


	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="personalmedical", orphanRemoval = true, cascade = CascadeType.PERSIST)
	private List<Programare> programares;

	/**
	 * Instantiates a new personal medical.
	 */
	public PersonalMedical() {
	}

	/**
	 * Gets the idPersonalMedical.
	 *
	 * @return the idPersonalMedical
	 */
	public int getIdpersonalmedical() {
		return this.idpersonalmedical;
	}

	/**
	 * Sets the idPersonalMedical.
	 *
	 * @param idPersonalMedical the new idPersonalMedical
	 */
	public void setIdpersonalmedical(int idpersonalmedical) {
		this.idpersonalmedical = idpersonalmedical;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}