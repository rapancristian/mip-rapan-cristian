/*
 * 
 */
package model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the Programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@EmbeddedId
	private ProgramarePK id;

	/** The Data. */
	private String data;

	/** The ora. */
	private String ora;

	/** The PersonalMedical. */
	//bi-directional many-to-one association to PersonalMedical
	@ManyToOne
	private PersonalMedical personalmedical;

	/** The animal. */
	//bi-directional many-to-one association to Animal
	@ManyToOne
	private Animal animal;

	/**
	 * Instantiates a new programare.
	 */
	public Programare() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public ProgramarePK getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(ProgramarePK id) {
		this.id = id;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public String getData() {
		return this.data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * Gets the ora.
	 *
	 * @return the ora
	 */
	public String getOra() {
		return this.ora;
	}

	/**
	 * Sets the ora.
	 *
	 * @param ora the new ora
	 */
	public void setOra(String ora) {
		this.ora = ora;
	}

	/**
	 * Gets the PersonalMedical.
	 *
	 * @return the PersonalMedical
	 */
	public PersonalMedical getPersonalmedical() {
		return this.personalmedical;
	}

	/**
	 * Sets the PersonalMedical.
	 *
	 * @param personalmedical the new PersonalMedical
	 */
	public void setPersonalmedical(PersonalMedical personalmedical) {
		this.personalmedical = personalmedical;
	}

	/**
	 * Gets the animal.
	 *
	 * @return the animal
	 */
	public Animal getAnimal() {
		return this.animal;
	}

	/**
	 * Sets the animal.
	 *
	 * @param animal the new animal
	 */
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	
	
	

}