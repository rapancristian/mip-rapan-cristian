package listener;

public class LongRunningTask implements Runnable {

    private OnCompleteListener onCompleteListener;

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5 * 1000);
            onCompleteListener.onComplete();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}