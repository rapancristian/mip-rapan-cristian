package unitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.PersonalMedical;

public class UnitClass {
	/*** Test function for animal names ***/
    @Test
    public void testAnimalName() {
        Animal testAnimal = new Animal();
        testAnimal.setName("Bob");
        assertEquals(testAnimal.getName(), "Bob");
    }
    
    @Test
    public void testAnimalFails() {
        Animal testAnimal = new Animal();
        testAnimal.setName("Damn");
        assertEquals(testAnimal.getName(), "Bob");
    }
    
    @Test
    public void testAnimalNameNotNull() {
        Animal testAnimal = new Animal();
        testAnimal.setName("Damn");
        assertNotEquals("", testAnimal.getName());
    }
    
    @Test
    public void testPersonalMedicalNotNull() {
        PersonalMedical testPersonal = new PersonalMedical();
        testPersonal.setName("Clarisa");
        assertEquals("", testPersonal.getName());
    }
    
    @Test
    public void testPersonalId() {
        PersonalMedical testPersonal = new PersonalMedical();
        testPersonal.setName("Andreea");
        assertEquals("Andreea", testPersonal.getName());
    }
    
    
    

}
