/*
 * 
 */
package util;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.PersonalMedical;
import model.Programare;
import model.ProgramarePK;

/**
 * The Class DataBaseUtil.
 */
public class DataBaseUtil {
	
	private static DataBaseUtil instance = new DataBaseUtil();
	public static DataBaseUtil getInstance() {
		return instance;
	}
	
	public static void setInstance(DataBaseUtil instance) {
		DataBaseUtil.instance = instance;
	}
	
	/** The entity manager factory. */
	public static EntityManagerFactory entityManagerFactory;
	
	/** The entity manager. */
	public static EntityManager entityManager;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Delete animal.
	 *
	 * @param idAnimal the id animal
	 */
	public void deleteAnimal(int idAnimal)
	{
		Animal toBeDeleted = entityManager.find(Animal.class,idAnimal);
		entityManager.remove(toBeDeleted);
	}
	
	public Animal findAnimal(int id) {
 		Animal myAnimal = entityManager.find(Animal.class, id);
 		return myAnimal;
 	}
	
 	public PersonalMedical findPersonalMedical(int id) {
 		PersonalMedical PersonalMedical = entityManager.find(PersonalMedical.class, id);
 		return PersonalMedical;
 	}
 	
 	public Programare findProgramare(int id) {
 		Programare programare = entityManager.find(Programare.class, id);
 		return programare;
 	}
 	
	/**
	 * Delete personal medical.
	 *
	 * @param idPersonalMedical the id personal medical
	 */
	public void deletePersonalMedical(int idPersonalMedical)
	{
		PersonalMedical toBeDeleted = entityManager.find(PersonalMedical.class,idPersonalMedical);
		entityManager.remove(toBeDeleted);
	}
	
	/**
	 * Delete programare.
	 *
	 * @param idProgramare the id programare
	 */
	public void deleteProgramare(int idProgramare)
	{
		Programare toBeDeleted = entityManager.find(Programare.class,idProgramare);
		entityManager.remove(toBeDeleted);
	}
	
	/**
	 * Save animal.
	 *
	 * @param animal the animal
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	/**
	 * Save personal medical.
	 *
	 * @param personalMedical the personal medical
	 */
	public void savePersonalMedical(PersonalMedical personalMedical) {
		entityManager.persist(personalMedical);
	}
	
	/**
	 * Save programare.
	 *
	 * @param programare the programare
	 */
	public void saveProgramare (Programare programare) {
		entityManager.persist(programare);
	}
	
	/**
	 * Start transaction.
	 */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * Commit transaction.
	 */
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	/**
	 * Close entity manager.
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	/**
	 * Prints the programari.
	 */
	public void printProgramari() {
		List<Programare> programari = entityManager.createNativeQuery("Select * from Petshop.Programare order by data, ora",Programare.class).getResultList();
		for (Programare programare : programari)
			//System.out.println("ID: " + programare.getId().getIdProgramare() + "DATA: "+ programare.getData()  + programare.getOra()  + programare.getAnimal().getName()  + programare.getPersonalMedical().getName());
			System.out.println(programare.getAnimal().getName() + " este programat la " + programare.getPersonalmedical().getName() + " pe " + programare.getData() + " ora: " + programare.getOra());
	}
	
	/**
	 * Prints the all animals from DB.
	 */
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from Petshop.Animal",Animal.class).getResultList();
	for (Animal animal : results) {
		System.out.println("Animal :" + animal.getName() + " has ID: " + animal.getIdAnimal());
	}
	}
	
	/**
	 * Prints the all personal medical from DB.
	 */
	public void printAllPersonalMedicalFromDB() {
		List<PersonalMedical> results = entityManager.createNativeQuery("Select * from Petshop.PersonalMedical",PersonalMedical.class).getResultList();
	for (PersonalMedical personal : results) {
		System.out.println("PersonalMedical :" + personal.getName() + " has ID: " + personal.getIdpersonalmedical());
	}
	
//SELECT a FROM animal a
//Select * from Petshop.Animal
	}
	
	public List<Animal> animalList(){
	    List<Animal> animalList = (List<Animal>)entityManager.
	    		createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
	    return animalList;
		}
		
		//returns a list of all personal medical
		public List<PersonalMedical> personalMedicalList(){
			List<PersonalMedical> personalMedicalList = (List<PersonalMedical>)entityManager.
					createQuery("SELECT p FROM PersonalMedical p",PersonalMedical.class).getResultList();
			return personalMedicalList;
		}
		
		//returns a list of all programari
		public List<Programare> programareList(){
			List<Programare> programareList = (List<Programare>)entityManager.
					//Select * from Petshop.Programare order by data, ora
					//SELECT p FROM Programare p
					createQuery("SELECT p FROM Programare p",Programare.class).getResultList();
			return programareList;
		}
		
		public List<ProgramarePK> programarePKList(){
			List<ProgramarePK> programareList = (List<ProgramarePK>)entityManager.
					createQuery("SELECT p FROM ProgramarePK p",ProgramarePK.class).getResultList();
			return programareList;
		}
		
		public Map<Integer, String> mapAnimals() {

	        List<Programare> ProgramList = entityManager
	                .createNativeQuery("Select * from Petshop.Programare order by data",Programare.class).getResultList();
	        Map<Integer, String> mapAnimals = new HashMap<>();

	        for (Programare p : ProgramList)

	        	mapAnimals.put(p.getAnimal().getIdAnimal(), p.getPersonalmedical().getName());

	        return mapAnimals;

	    }
		
		public Set<String> setAnimal() {

			Set<String> set = new HashSet<String>();
          
           
			List<Animal> animalList =  DataBaseUtil.getInstance().animalList();
			for (Animal animal : animalList) {
				set.add(animal.getName());
			}

			return set;
		}
		
		
		public <T> void printEntityClass(T entity) {
			System.out.println(entity.getClass());
		}
		
		public void orderPersonalMedical(List<? extends PersonalMedical> personalMedicalOrderList) {

			Collections.sort(personalMedicalOrderList, (personal1, personal2) -> personal1.getName().compareTo(personal2.getName()));
			System.out.println(personalMedicalOrderList);
		}

}
