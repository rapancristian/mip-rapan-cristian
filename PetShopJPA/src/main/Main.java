/*
 * 
 */
package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import listener.LongRunningTask;
import listener.OnCompleteListener;
import util.DataBaseUtil;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try
		{
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/LoginView.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			//
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
            ServerSocket serverSocket = new ServerSocket(5000);
            while (true) {
                new server.Server(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
		
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		

        
		launch(args);
		 LongRunningTask longRunningTask = new LongRunningTask();
	        longRunningTask.setOnCompleteListener(new OnCompleteListener() {
	            
	        /* Sort all appointments from DB */

	         public void onComplete() {
	         
	             System.out.println("Yeah, the long running task has been completed!");
	             try {
					DataBaseUtil.getInstance().setUp();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	             DataBaseUtil.getInstance().startTransaction();
	             DataBaseUtil.getInstance().commitTransaction();
	             DataBaseUtil.getInstance().printProgramari();
	             
	         }});
		
	        System.out.println("Starting the long running task.");
	        longRunningTask.run();
	        
			/*
			Map<Integer,String> map = DataBaseUtil.getInstance().mapAnimals();
	        for(Map.Entry<Integer, String> entry : map.entrySet()) {
	            System.out.println("Animal " + entry.getKey() + " -> " + entry.getValue());
	        }
	        */
		//DataBaseUtil dbUtil = new DataBaseUtil();
		//dbUtil.setUp();
		
/*		<<<<<<< CREATE >>>>>>>
		Animal rusky = new Animal();
				rusky.setIdAnimal(1);
				rusky.setName("rusky");
		PersonalMedical alex  = new PersonalMedical();
			 alex.setIdPersonalMedical(2);
			 alex.setName("Alex");
		Programare andrew = new Programare();
		andrew.setData("3.1.2019");
		andrew.setOra("13");
		andrew.setAnimal(rusky);
		andrew.setPersonalmedical(alex);
		andrew.setId(7);*/		
		
/*		 <<<<<<< READ >>>>>>>
		dbUtil.printAllAnimalsFromDB();
		dbUtil.printAllPersonalMedicalFromDB();

		
/*		 <<<<<<< UPDATE >>>>>>>
		dbUtil.saveAnimal(rusky);
		dbUtil.savePersonalMedical(alex);
		dbUtil.saveProgramare(andrew);
		dbUtil.commitTransaction();*/
	
/*		<<<<<<< DELETE >>>>>>>
		dbUtil.startTransaction();
		dbUtil.deleteAnimal(9);
		dbUtil.commitTransaction();*/
		//dbUtil.printProgramari();
		//dbUtil.closeEntityManager();
	}



}
